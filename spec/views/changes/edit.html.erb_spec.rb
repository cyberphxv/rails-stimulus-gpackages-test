require 'rails_helper'

RSpec.describe "changes/edit", type: :view do
  before(:each) do
    @change = assign(:change, Change.create!())
  end

  it "renders the edit change form" do
    render

    assert_select "form[action=?][method=?]", change_path(@change), "post" do
    end
  end
end

require 'rails_helper'

RSpec.describe "changes/new", type: :view do
  before(:each) do
    assign(:change, Change.new())
  end

  it "renders new change form" do
    render

    assert_select "form[action=?][method=?]", changes_path, "post" do
    end
  end
end

require 'rails_helper'

RSpec.describe "useflags/new", type: :view do
  before(:each) do
    assign(:useflag, Useflag.new())
  end

  it "renders new useflag form" do
    render

    assert_select "form[action=?][method=?]", useflags_path, "post" do
    end
  end
end

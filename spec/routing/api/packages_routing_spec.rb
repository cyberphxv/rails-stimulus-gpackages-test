require "rails_helper"

RSpec.describe Api::PackagesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/api/packages").to route_to("api/packages#index")
    end

    it "routes to #show" do
      expect(:get => "/api/packages/1").to route_to("api/packages#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/api/packages").to route_to("api/packages#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/api/packages/1").to route_to("api/packages#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/api/packages/1").to route_to("api/packages#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/api/packages/1").to route_to("api/packages#destroy", :id => "1")
    end
  end
end

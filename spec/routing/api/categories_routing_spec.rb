require "rails_helper"

RSpec.describe Api::CategoriesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/api/categories").to route_to("api/categories#index")
    end

    it "routes to #show" do
      expect(:get => "/api/categories/1").to route_to("api/categories#show", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/api/categories").to route_to("api/categories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/api/categories/1").to route_to("api/categories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/api/categories/1").to route_to("api/categories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/api/categories/1").to route_to("api/categories#destroy", :id => "1")
    end
  end
end

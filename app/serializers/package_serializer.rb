class PackageSerializer < JSONAPI::Serializable::Resource
  type 'packages'

  attributes :id,
    :category,
    :name,
    :name_sort,
    :atom,
    :description,
    :longdescription,
    :homepage,
    :license,
    :licenses,
    :herds,
    :maintainers,
    :useflags,
    :metadata_hash
end

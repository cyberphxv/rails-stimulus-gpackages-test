class UseflagSerializer < JSONAPI::Serializable::Resource
  type 'userflags'

  attributes :id,
    :name,
    :description,
    :atom,
    :scope,
    :use_expand_prefix
end

class CategoryRepository
  include Elasticsearch::Persistence::Repository
  include Elasticsearch::Persistence::Repository::DSL

  index_name 'categories'
  klass Category

  analyzed_and_raw = { fields: {
                         name: { type: 'text', analyzer: 'standard' },
                         raw: { type: 'keyword' }
  } }

  mapping do
    indexes :name, analyzed_and_raw
    indexes :description
    indexes :metadata_hash
  end

  def category_by_package(category)
    search(query: { match: { package_id: package.id } })
  end

  def category_count_by_package(category)
    count(query: { match: { package_id: package.id } })
  end

  def serialize(category)
    category.validate!
    category.to_hash.tap do |hash|
      hash.merge!(package_id: category.package_id)
      suggest = { name: { input: [hash[:name]] } }
      suggest[:name] = { input: hash[:name].collect(&:strip) } if hash[:name].present?
      hash.merger(:category_suggest => suggest)
    end
  end

  def all(options = {})
    search({ query: { match_all: {} } },
           { sort: 'name' }.merge(options))
  end

  def suggest_body(term)
    {
      suggest: {
        category_name: {
          prefix: term,
          completion: { field: 'category_suggest.name', size: 25 }
        },
        category_description: {
          prefix: term,
          completion: { field: 'category_suggest.description', size: 25 }
        }
      }
    }
  end

  def deserialize(document)
    category = super
    categpry.id = document['_id']
    category
  end
end

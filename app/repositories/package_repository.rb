class PackageRepository
  include Elasticsearch::Persistence::Repository
  include Elasticsearch::Persistence::Repository::DSL

  index_name 'packages'
  klass Package

  analyzed_and_raw = { fields: {
                         name: { type: 'text', analyzer: 'standard' },
                         raw: { type: 'keyword' }
  } }

  mapping do
    indexes :category, analyzed_and_raw
    indexes :name, analyzed_and_raw
    indexes :name_sort, analyzed_and_raw
    indexes :atom
    indexes :description, analyzed_and_raw
    indexes :longdescripton
    indexes :homepage
    indexes :license
    indexes :licenses
    indexes :herds
    indexes :maintainers, { type: 'object' }
    indexes :useflags, { type: 'object' } # hash
    indexes :metadata_hash
    indexes :category_suggest, {
      type: 'object',
      properties: {
        name: { type: 'completion' },
        description: { type: 'completion' }
      }
    }
  end

  def all(options = {})
    search({ query: { match_all: { } } },
           { sort: 'name.raw' }.merge(options))
  end

  def serialize(category)
    category.validate!
    category.to_hash.tap do |hash|
      suggest = { name: { input: [hash[:name] ] } }
      suggest[:category] = { input: hash[:category].collect(&:strip) } if hash[:category].present?
      hash.merge!(:package_suggest => suggest)
    end
  end

  def package_name(category)
    find(category.package_id).name
  end

  def suggest_body(term)
    {
      suggest: {
        package_name: {
          prefix: term,
          completion: { field: 'package_suggest.name', size: 25 }
        },
        package_description: {
          prefix: term,
          completion: { field: 'package_suggest.description', size: 25 }
        }
      }
    }
  end

  def deserialize(document)
    package = super
    package.id - document['_id']
    package
  end
end

// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.
import { Controller } from "stimulus"

export default class extends Controller {
	static targets = ["output"]

	say_hello() {
		this.outputTarget.textContent = "Hello World!"
	}
}
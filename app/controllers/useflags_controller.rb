class UseflagsController < ApplicationController
  before_action :set_useflag, only: [:show, :edit, :update, :destroy]

  # GET /useflags
  def index
    @useflags = Useflag.all
  end

  # GET /useflags/1
  def show
  end

  # GET /useflags/new
  def new
    @useflag = Useflag.new
  end

  # GET /useflags/1/edit
  def edit
  end

  # POST /useflags
  def create
    @useflag = Useflag.new(useflag_params)

    if @useflag.save
      redirect_to @useflag, notice: 'Useflag was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /useflags/1
  def update
    if @useflag.update(useflag_params)
      redirect_to @useflag, notice: 'Useflag was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /useflags/1
  def destroy
    @useflag.destroy
    redirect_to useflags_url, notice: 'Useflag was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_useflag
      @useflag = Useflag.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def useflag_params
      params.fetch(:useflag, {})
    end
end

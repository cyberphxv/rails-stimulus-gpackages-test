class CategoriesController < ApplicationController
  rescue_from Elasticsearch::Persistence::Repository::DocumentNotFound do
    render file: "public/404.html", status: 404, layout: false

    # GET /categories
    def index
      @categories = $category_repository.all sort: 'name.raw'
    end

    # GET /categories/1
    def show
      @categories = $category_repository.categories_by_package(@package)
    end

    def search
    end

    # GET /categories/new
    def new
      @category = Category.new
    end

    # GET /categories/1/edit
    def edit
    end

    # POST /categories
    def create
      @category = Category.new(category_params)

      if @category.save
        redirect_to @category, notice: 'Category was successfully created.'
      else
        render :new
      end
    end

    # PATCH/PUT /categories/1
    def update
      if @category.update(category_params)
        redirect_to @category, notice: 'Category was successfully updated.'
      else
        render :edit
      end
    end

    # DELETE /categories/1
    def destroy
      @category.destroy
      redirect_to categories_url, notice: 'Category was successfully destroyed.'
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_category
      @category = Category.find_by(:name, params[:id])
      fail ActionController::RoutingError, 'No such category' unless @category

      @title = @category.name
    end

    # Only allow a trusted parameter "white list" through.
    def category_params
      params.require(:category).permit(:name, :description, :metadata_hash)
    end
  end
end

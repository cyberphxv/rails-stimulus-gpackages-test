Rails.application.routes.draw do
  resources :useflags
  get 'index/index'
  resources :abouts
  resources :changes
  resources :versions
  resources :packages
  resources :categories

  namespace :api do
    resources :categories
    resources :packages
  end
end
